# NPS

- [ ] Migrate Bloggo to NPS

[NPM package](https://www.npmjs.com/package/nps)

You define a `package-scripts.js` file and then run `nps $name` to run the script.

Saves you from bloated `package.json` with heaps of scripts!

Cool.
